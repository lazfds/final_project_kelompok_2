<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jawaban;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jawaban = Jawaban::all();
        return view('jawaban.index', compact('jawaban'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jawaban = Jawaban::all();
        return view('jawaban.create', compact('jawaban'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'judul' => 'required',
            'jawaban' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->gambar->extension();

        $request->gambar->move(public_path('gambar'), $imageName);

        $jawaban = new Jawaban;
        $jawaban->judul = $request->judul;
        $jawaban->jawaban = $request->jawaban;
        $jawaban->gambar = $imageName;
        $jawaban->save();

        return redirect ('/jawaban');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $jawaban = Jawaban::find($id);
        return view('jawaban.show', compact('jawaban'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $jawaban = Jawaban::find($id);
        return view('jawaban.edit', compact('jawaban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
        $request->validate([
            'judul' => 'required',
            'jawaban' => 'required',
        ]);

        $jawaban = Jawaban::find($id);
        $jawaban->judul = $request->judul;
        $jawaban->jawaban = $request->jawaban;
        $jawaban->update();

        return redirect('/jawaban');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $jawaban = Jawaban::find($id);
        $jawaban->delete();
        return redirect('/jawaban');
    }
}
