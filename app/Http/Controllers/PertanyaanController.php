<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pertanyaan;
use App\Kategori;
use File;

class PertanyaanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('pertanyaan.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'tulisan' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_id' => 'required',

        ]);

        $imageName = time().'.'.$request->gambar->extension();

        $request->gambar->move(public_path('gambar'), $imageName);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->tulisan = $request->tulisan;
        $pertanyaan->gambar = $imageName;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();

        return redirect ('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        // views($pertanyaan)->record();

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = Kategori::all();
        return view('pertanyaan.edit', compact('pertanyaan', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $request->validate([
            'judul' => 'required',
            'tulisan' => 'required',
            'gambar' => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_id' => 'required',

        ]);

        if ($request->has('gambar')){
            //$post->delete();
            $path = "gambar/";
            File::delete($path . $pertanyaan->gambar);
            $imageName = time().'.'.$request->gambar->extension();

            $request->gambar->move(public_path('gambar'), $imageName);

            $pertanyaan->gambar = $imageName;
        }

        $pertanyaan->judul = $request->judul;
        $pertanyaan->tulisan = $request->tulisan;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();

        return redirect ('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::findorfail($id);
        $pertanyaan->delete();

        $path = "gambar/";
        File::delete($path . $pertanyaan->gambar);

        return redirect('/pertanyaan');
    }
}
