<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostinganJawaban;
use App\Jawaban;
use App\User;

class PostinganJawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $postingan_jawaban = PostinganJawaban::all();
        return view('postingan_jawaban.index', compact('postingan_jawaban'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $jawaban = Jawaban::all();
        return view('postingan_jawaban.create', compact('jawaban'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'users_id' => 'required',
            'jawaban_id' => 'required',
        ]);

        PostinganJawaban::create([
            'users_id' => $request->users_id,
            'jawaban_id' => $request->jawaban_id,
        ]);

        return redirect ('/jawaban');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postingan_jawaban = PostinganJawaban::find($id);
        // views($postingan_jawaban)->record();

        return view('postingan_jawaban.show', compact('postingan_jawaban'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $postingan_jawaban = PostinganJawaban::find($id);
        $user = User::all();
        $jawaban = Jawaban::all();
        return view('postingan_jawaban.edit', compact('postingan_jawaban', 'jawaban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $postingan_jawaban = PostinganJawaban::find($id);
        $postingan_jawaban->delete();
        return redirect('/postingan_jawaban');
    }
}
