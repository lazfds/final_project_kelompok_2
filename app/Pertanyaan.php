<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{

    protected $table = 'pertanyaan';

    protected $fillable = ['judul', 'tulisan', 'gambar', 'kategori_id', 'user_id'];

    /**
     * The kategori that belong to the Pertanyaan
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function kategori()
    {
        return $this->belongsToMany('App\Kategori', 'kategori_id');
    }

    /**
     * The user that belong to the Pertanyaan
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'user_id');
    }
}
