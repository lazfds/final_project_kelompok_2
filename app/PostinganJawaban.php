<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PostinganJawaban extends Model implements Viewable
{

    protected $table = 'postingan_jawaban';

    protected $guarded = [];

    /**
     * The jawaban that belong to the PostinganJawaban
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jawaban()
    {
        return $this->belongsToMany('App\Jawaban', 'jawaban_id');
    }

    /**
     * The user that belong to the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'user_id');
    }

}
