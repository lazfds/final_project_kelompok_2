<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['nama', 'gender', 'alamat', 'biodata', 'umur', 'user_id'];

}
