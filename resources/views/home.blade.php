@extends('layouts.frontend')

@section('content')

<div class="row mt-auto">
    <div class="container col-12 col-md-8 mt-md-5 diskusi">
        <!-- <h2>Hoverable Dark Table</h2>
        <p>The .table-hover class adds a hover effect (grey background color) on table rows:</p> -->
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="urutkan"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Urutkan
            </button>
            <div class="dropdown-menu" aria-labelledby="urutkan">
                <button class="dropdown-item" type="button">Terbaru</button>
                <button class="dropdown-item" type="button">Terlama</button>
                <button class="dropdown-item" type="button">Populer</button>
            </div>
        </div>

        <table class="table table-dark table-hover mt-md-5">
            <thead>
                <tr>
                    <th>Pertanyaan</th>
                    <th>Kategori</th>
                    <th>User</th>
                    <th><i class="fas fa-eye"></i> Dilihat</th>
                    <th><i class="fas fa-comment"></i> Komentar</th>
                </tr>
            </thead>
        @foreach ($pertanyaan as $item)
            <tbody>
                <tr>
                    <td><a href="/frontend">{{$pertanyaan->judul}}</a></td>
                    <td><a href="/frontend/kategori" class="badge badge-primary">{{$kategori->nama}}</a></td>
                    <td><a href="/profile">{{$profile}}</a></td>
                    <td>5 view</td>
                    <td>1 Komentar</td>
                </tr>
            </tbody>
        @endforeach
        </table>


        <ul class="pagination justify-content-center">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul>



    </div>

    <div class="container col-6 col-md-4 mt-md-5 sidebar">
        <div class="container">
            <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal"
                data-target="#posting_pertanyaan">Posting Pertanyaan Baru</button>
            <br>
            <p>Kategori</p>
            <button type="button" class="btn btn-outline-warning btn-lg btn-block">Block level
                button</button>
            <button type="button" class="btn btn-outline-warning btn-lg btn-block">Block level
                button</button>
            <button type="button" class="btn btn-outline-warning btn-lg btn-block">Block level
                button</button>
            <button type="button" class="btn btn-outline-warning btn-lg btn-block">Block level
                button</button>
        </div>
    </div>
</div>
<!-- New Thread Modal -->
<div class="modal fade" id="posting_pertanyaan" tabindex="-1" role="dialog"
    aria-labelledby="thread_pertanyaan" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header d-flex align-items-center bg-primary text-white">
                    <h6 class="modal-title mb-0" id="thread_pertanyaan">Pertanyaan Baru</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="judul_pertanyaan">Judul</label>
                        <input type="text" class="form-control" id="judul_pertanyaan"
                            name="judul_pertanyaan" placeholder="Masukkan judul" autofocus="" />
                    </div>

                    <textarea class="form-control summernote" id="tulisan_pertanyaan"
                        name="tulisan_pertanyaan" placeholder="Masukkan Pertanyaan"
                        style="resize:none;"></textarea>

                    <div class="custom-file form-control-sm mt-3" style="max-width: 300px;">
                        <input type="file" class="custom-file-input" id="gambar_pertanyaan"
                            name="tulisan_pertanyaan" multiple="" />
                        <label class="custom-file-label" for="gambar_pertanyaan">Gambar (maksimal 1
                            file)</label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"
                        id="batal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="submit_pertanyaan">Post</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
