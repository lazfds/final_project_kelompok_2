@extends('layouts.master')
@section('title')
<h1>Halaman Jawaban</h1>
@endsection

@section('content')

    <form action="/jawaban" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jawaban">Jawaban</label>
            <textarea name="jawaban" class='form-control' id="jawaban" cols="30" rows="10"></textarea>
            @error('jawaban')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="gambar">Gambar</label>
            <input type="file" class="form-control" name="gambar" id="gambar" placeholder="Masukan Gambar">
            @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

    @push('scripts')
        <script src="https://cdn.tiny.cloud/1/otwokmacz07k5tb2vdrafl2j3vp4sn4r3lbddo634aqwcvvt/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
        <script>
            tinymce.init({
                    selector: 'textarea',
                    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                    toolbar_mode: 'floating',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                    resize: false
                    });
        </script>
    @endpush

@endsection
