@extends('layouts.master')
@section('title')
<h1>Halaman Edit Jawaban</h1>
@endsection

@section('content')
        <form action="/jawaban/{{$jawaban->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" value="{{$jawaban->judul}}" id="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="jawaban">Isi jawaban</label>
                <textarea name="jawaban" class='form-control' id="jawaban" cols="30" rows="10">value="{{$jawaban->jawaban}}"</textarea>
                @error('jawaban')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gambar">Gambar</label>
                <input type="file" class="form-control" name="gambar">
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection
