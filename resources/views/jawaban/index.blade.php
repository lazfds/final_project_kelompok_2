@extends('layouts.master')
@section('title')
<h1>Halaman List Jawaban</h1>
@endsection

@section('content')
<a href="/jawaban/create" class="btn btn-btn primary my-2">Tambah Jawaban</a>
<div class="row">
    @forelse ($jawaban as $item)
    <div class="col-12">
        <div class="card">
            <img src="{{asset('gambar/'.$item->gambar)}}" width="100%" height="300px" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title">{{$item->judul}}</h3>
              <p class="card-text">{{$item->tulisan}}</p>
              <form action="/jawaban/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="{{ route('jawaban.show', ['jawaban' => $item->id]) }}" class="btn btn-primary">Detail</a>
                    <a href="{{ route('jawaban.edit', ['jawaban' => $item->id]) }}" class="btn btn-primary">Edit</a>
                    <input type="submit" value="delete" class="btn btn-danger">
              </form>

            </div>
        </div>
    </div>
    @empty
        <h1>Tidak ada jawaban</h1>
    @endforelse
</div>
@endsection
