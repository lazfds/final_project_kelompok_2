@extends('layouts.master')
@section('title')
<h1>Halaman Jawaban</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <img src="{{ asset('gambar/'.$jawaban->gambar) }}" width="100%" height="300px" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title">{{ $jawaban->judul }}</h3>
              <p class="card-text">{{ $jawaban->tulisan }}</p>
              <a href="/jawaban" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
</div>
@endsection
