@extends('layouts.master') 
@section('title')
<h1>Halaman Kategori</h1>
@endsection

@section('content')
        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Kategori</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama Kategori">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tag">Tag</label>
                <input type="text" class="form-control" name="tag" id="tag" placeholder="Masukkan Tag">
                @error('tag')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection