@extends('layouts.master') 
@section('title')
<h1>Edit Kategori</h1>
@endsection

@section('content')
        <h2>Edit Kategori{{$kategori->id}}</h2>
        <form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama Kategori</label>
                <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}" id="nama" placeholder="Masukkan nama Kategori">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tag">Tag</label>
                <input type="text" class="form-control" name="tag" value="{{$kategori->tag}}" id="tag" placeholder="Masukkan Tag">
                @error('tag')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
@endsection