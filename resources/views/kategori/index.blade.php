@extends('layouts.master') 
@section('title')
<h1>Halaman Kategori</h1>
@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>
@endpush

@section('content')
<a href="/kategori/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">tag</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->tag}}</td>
                <td>
                    <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/kategori/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection