@extends('layouts.master') 
@section('title')
    Halaman Detail Kategori {{$kategori->id}}
@endsection

@section('content')

<h2 class="text-primary">{{$kategori->nama}} ({{$kategori->tag}})</h2>
@endsection