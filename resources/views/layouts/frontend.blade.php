<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forum Diskusi</title>
    <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <!-- <link rel="stylesheet" href="scss/style.scss"> -->
    <link rel="stylesheet" href="{{ asset("css/style.css") }}">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
        crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/fe8270cc7d.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- header -->
    <header>
        <div class="container-flex">
            <!-- additional navigation -->
            <!-- navigation bar (header menu) -->
            <nav class="navbar navbar-expand-lg navbar-custom navigasi">
                <div class=""></div>
                <a class="navbar-brand" href="#">Forum Diskusi</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#"><b>HOME</b> <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><b>MEMBER</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><b>ARTIKEL</b></a>
                        </li>

                        <!-- Authentication Links -->
                        @guest
                            {{-- Jika belum login --}}
                            <li class="nav-item login">
                                <button type="button" class="btn btn-danger btn-sm">
                                    <a class="nav-link" href="{{ route('login') }}"><b>LOGIN</b></a></a>
                                </button>
                            </li>

                            @if (Route::has('register'))
                                <li class="nav-item daftar">
                                    <button type="button" class="btn btn-success btn-sm">
                                        <a class="nav-link" href="{{ route('register') }}"><b>DAFTAR</b></a></button>
                                </li>
                                <li class="nav-item">

                                </li>
                            @endif

                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest

                    </ul>
                    <!-- <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form> -->
                </div>
            </nav>
        </div>
    </header>

    <section id="cari_diskusi">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="d-flex flex-grow-1">
                <form class="form-inline mr-2 my-auto w-100 d-inline-block order-1 cari" action="" method="post">
                    <div class="input-group">
                        <input class="form-control form-control-md" type="text" placeholder="Cari pertanyaan di sini"
                            aria-label="Search">
                        <span class="input-group-append">
                            <button class="btn btn-outline-light border border-left-0" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
            <button class="navbar-toggler order-0" type="button" data-toggle="collapse" data-target="#navbar7">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse flex-shrink-1 flex-grow-0 order-last" id="navbar7">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <button type="button" class="btn btn-outline-info btn-md dropdown">Kategori</button>
                    </li>
                </ul>
            </div>
        </nav>
    </section>

    <main>
        <div class="container-fluid">
            @yield('content')
        </div>
    </main>

    <footer class="text-center text-white fixed-bottom" style="background-color: #f1f1f1;">
        <!-- Grid container -->
        <div class="container pt-2">
            <!-- Section: Social media -->
            <section class="mb-2">
                <!-- Facebook -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-twitter"></i></a>

                <!-- Google -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-google"></i></a>

                <!-- Instagram -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-instagram"></i></a>

                <!-- Linkedin -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-linkedin"></i></a>
                <!-- Github -->
                <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="#!" role="button"
                    data-mdb-ripple-color="dark"><i class="fab fa-github"></i></a>
            </section>
            <!-- Section: Social media -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center text-dark p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2022 Copyright:
            <a class="text-dark" href="">Kelompok 2</a>
        </div>
        <!-- Copyright -->
    </footer>


</body>

</html>
