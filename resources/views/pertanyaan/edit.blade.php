@extends('layouts.master')
@section('title')
<h1>Halaman Edit Pertanyaan</h1>
@endsection

@section('content')
        <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tulisan">Isi Pertanyaan</label>
                <textarea name="tulisan" class='form-control' id="tulisan" cols="30" rows="10">value="{{$pertanyaan->tulisan}}"</textarea>
                @error('tulisan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gambar">Gambar</label>
                <input type="file" class="form-control" name="gambar">
            </div>
            <div class="form-group">
                <label for="kategori_id">Kategori</label>
                <select nama="kategori_id" class="form-control">
                    <option value="">--Pilih Kategori--</option>
                    @foreach ($kategori as $item)
                        @if ($item->id === $pertanyaan->kategori_id)

                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>

                        @endif
                    @endforeach
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection
