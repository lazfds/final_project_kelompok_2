@extends('layouts.master')
@section('title')
<h1>Halaman Detail Pertanyaan</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <img src="{{ asset('gambar/'.$pertanyaan->gambar) }}" width="100%" height="300px" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title">{{ $pertanyaan->judul }}</h3>
              <p class="card-text">{{ $pertanyaan->tulisan }}</p>
              <a href="/pertanyaan" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
</div>
@endsection
