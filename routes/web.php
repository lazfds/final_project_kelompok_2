<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('kategori', 'KategoriController');
    Route::resource('pertanyaan', 'PertanyaanController');
    Route::resource('jawaban', 'JawabanController');
    Route::resource('postingan_jawaban', 'PostinganJawabanController');
    Route::resource('profile', 'ProfileController');
    Route::resource('users', 'UserController');

    Route::get('/', function () {
        return view('erd');
    });
});

Auth::routes();
